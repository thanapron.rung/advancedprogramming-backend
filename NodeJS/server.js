var http = require('http')
var date = new Date();
http
  .createServer(function (req, res) {
    res.writeHead(200, { 'Content-Type': 'text/plain' })
    res.end("Thanaporn (Fon) Rungwittayanon\n" + date.toLocaleString());
  })
  .listen(2337, '127.0.0.1')

console.log('Server running at http://127.0.0.1:2337/')



