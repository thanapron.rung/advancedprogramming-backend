var alphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
var alphabet_switch = "";

try {
    main();
} catch (error) {
    console.log("Error name: " + error.name);
    console.log("Error message: " + error.message);
}

function switchAlphabet() {
for (let i = 0; i < alphabet.length; i++) {
        if (i % 2 != 0) {
            alphabet_switch += alphabet[i];
            alphabet_switch += alphabet[i-1];
        }
    }
    return alphabet_switch;
}

async function main() {
    let s = await switchAlphabet();
    console.log(s);
}