const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;
const ngrok = require('ngrok');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

let users = require('./userInfo.json');

function getindex(searchid) {
    const index = users.findIndex(function (user) {
        return user.id == searchid;
    });
    if (index === -1) {
        throw ''
    } else {
        return index
    }
}

function exists(type, search) {
    if (type === "email") {
        return users.some(user => user.email.includes(search));
    } else {
        return users.some(user => user.phone.includes(search));
    }
}

function checkundefined(check){
    if (typeof check === 'undefined'){
        throw '' 
    }
}

app.get('/users', function (req, res, next) {
    return res.status(200).json({
        code: 1,
        message: 'OK',
        data: users
    })
});

app.get('/user/:id', function (req, res, next) {
    try {
        let index = getindex(req.params.id)
        return res.status(200).json({
            code: 1,
            message: 'OK',
            data: users[index]
        })
    } catch {
        return res.status(400)
    }
});

app.post('/user', function (req, res, next) {
    try {
        let user = {}
        user.id = users.length + 1
        user.name = req.body.name;
        checkundefined(user.name)
        user.surname = req.body.surname;
        checkundefined(user.name)
        user.employeeid = req.body.employeeid;
        checkundefined(user.name)
        user.position = req.body.position;
        checkundefined(user.name)
        user.phone = req.body.phone;
        checkundefined(user.name)
        user.email = req.body.email;
        checkundefined(user.name)
        if (!exists("email", user.email) && !exists("phone", user.phone)) {
            users.push(user);
            return res.status(201).json({
                code: 1,
                message: 'OK',
                data: users
            });
        } else {
            return res.status(400).send("ข้อมูลไม่ถูกต้อง")
        }
    } catch {
        return res.status(400).send("ข้อมูลไม่ถูกต้อง")
    }
});

app.delete('/user/:id', function (req, res, next) {
    try {
        let index = getindex(req.params.id)
        users.splice(index, 1);
        return res.status(200).json({
            code: 1,
            message: 'OK',
            data: users
        })
    } catch {
        return res.status(400).send("ข้อมูลไม่ถูกต้อง")
    }
});

app.put('/user/:id', function (req, res, next) {
    try {
        let index = getindex(req.params.id)
        if(req.body.name != undefined && req.body.surname != undefined){
            return res.status(400).send("ข้อมูลไม่ถูกต้อง")
        }
        users[index].employeeid = req.body.employeeid;
        users[index].position = req.body.position;
        users[index].phone = req.body.phone;
        users[index].email = req.body.email;
        return res.status(200).json({
            code: 1,
            message: 'OK',
            data: users
        })
    } catch {
        return res.status(400).send("ข้อมูลไม่ถูกต้อง")
    }
});

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}`);
    (async function () {
        const url = await ngrok.connect(port);
        console.log(`Ngrok tunnel avalible at ${url}`)
    })();
});
