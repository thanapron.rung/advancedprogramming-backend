const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;
const ngrok = require('ngrok');
const oracledb = require('oracledb');

var ip = '45.32.119.244'
var password = '12345678'

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var setting = {
    user: "gosoft2",
    password: password,
    connectString: ip
}

async function oracleQuery(query, commit) {
    try {
        connection = await oracledb.getConnection(setting);
        if (typeof commit !== 'undefined') {
            result = await connection.execute(query, { autoCommit: true });
        } else {
            result = await connection.execute(query);
        }
        return result
    } catch (err) {
        console.error(err.message);
    } finally {
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                console.error(err.message);
            }
        }
    }
}

async function exists(type, search) {
    if (type === "email") {
        var respond = await oracleQuery(`
        SELECT 
            max(email) 
        FROM 
            employees 
        WHERE 
            email='${search}'`);
        return !!respond.rows[0][0]
    } else {
        var respond = await oracleQuery(`
        SELECT 
            max(phone) 
        FROM 
            employees 
        WHERE 
            phone='${search}'`);
        return !!respond.rows[0][0]
    }
}

function checkundefined(check) {
    if (typeof check === 'undefined') {
        throw ''
    }
}

app.get('/oauth2/token', function (req, res, next) {
    var headers = req.headers;
    if (headers.username == undefined || headers.password == undefined) {
        return res.status(403).json({
            message: 'FORBIDDEN'
        })
    } else {
        console.log(headers.username);
        if (headers.username == "test" && headers.password == "123456") {
            return res.status(200).json({
                token: "23399cf34d3fd1a3a317c8b86c86cf3b66a4f43f2ddadbe47d2ea880"
            })
        } else {
            return res.status(403).json({
                message: 'FORBIDDEN'
            })
        }
    }
});

app.get('/users', async function (req, res, next) {
    var headers = req.headers;
    if (headers.authorization == "Bearer 23399cf34d3fd1a3a317c8b86c86cf3b66a4f43f2ddadbe47d2ea880") {
        var users = await oracleQuery(`
    SELECT 
        * 
    FROM 
        employees`);
        return res.status(200).json({
            code: 1,
            message: 'OK',
            data: users
        })
    } else {
        return res.status(403).json({
            message: 'FORBIDDEN'
        })
    }
});

app.get('/user/:id', async function (req, res, next) {
    try {
        let id = req.params.id
        var headers = req.headers;
        if (headers.authorization == "Bearer 23399cf34d3fd1a3a317c8b86c86cf3b66a4f43f2ddadbe47d2ea880") {
            var user = await oracleQuery(`
            SELECT 
                * 
            FROM 
                employees 
            WHERE
                id='${id}'
            `);
            if (typeof user.rows[0] === "undefined") {
                return res.status(400).send("ไม่มี User ID ดังกล่าว")
            }
            return res.status(200).json({
                code: 1,
                message: 'OK',
                data: user
            })
        } else {
            return res.status(403).json({
                message: 'FORBIDDEN'
            })
        }
    } catch {
        return res.status(400)
    }
});

app.post('/user', async function (req, res, next) {
    try {
        var headers = req.headers;
        if (headers.authorization == "Bearer 23399cf34d3fd1a3a317c8b86c86cf3b66a4f43f2ddadbe47d2ea880") {
            let user = {}
            user.name = req.body.name;
            checkundefined(user.name)
            user.surname = req.body.surname;
            checkundefined(user.surname)
            user.title = req.body.title;
            checkundefined(user.title)
            user.phone = req.body.phone;
            checkundefined(user.phone)
            user.email = req.body.email;
            checkundefined(user.email)

            user.id = await oracleQuery(`
            SELECT 
                id
            FROM 
                employees
            WHERE
                id=(select max(id) from employees)`);
            user.id = user.id.rows[0][0] + 1

            var emailExist = await exists("email", user.email)
            var phoneExist = await exists("phone", user.phone)

            if (!emailExist && !phoneExist) {
                try {
                    connection = await oracledb.getConnection(setting);
                    result = await connection.execute("INSERT INTO employees VALUES (:id, :name, :surname, :title, :phone, :email)",
                        {
                            id: user.id,
                            name: user.name,
                            surname: user.surname,
                            title: user.title,
                            phone: user.phone,
                            email: user.email
                        }, { autoCommit: true });
                } catch (err) {
                    console.error(err.message);
                } finally {
                    if (connection) {
                        try {
                            await connection.close();
                        } catch (err) {
                            console.error(err.message);
                        }
                    }
                }
                var users = await oracleQuery(`
                SELECT 
                    * 
                FROM 
                    employees`);
                return res.status(201).json({
                    code: 1,
                    message: 'OK',
                    data: users
                });
            } else {
                return res.status(400).send("ข้อมูลไม่ถูกต้อง")
            }
        } else {
            return res.status(403).json({
                message: 'FORBIDDEN'
            })
        }
    } catch {
        return res.status(400).send("ข้อมูลไม่ถูกต้อง")
    }
});

app.delete('/user/:id', async function (req, res, next) {
    try {
        var headers = req.headers;
        if (headers.authorization == "Bearer 23399cf34d3fd1a3a317c8b86c86cf3b66a4f43f2ddadbe47d2ea880") {
            var id = req.params.id;
            try {
                connection = await oracledb.getConnection(setting);
                result = await connection.execute("DELETE FROM employees WHERE id=(:id)",
                    {
                        id: id,
                    }, { autoCommit: true });
                return res.status(200).json({
                    code: 1,
                    message: 'OK',
                    data: users
                })
            } catch (err) {
                console.error(err.message);
            } finally {
                if (connection) {
                    try {
                        await connection.close();
                    } catch (err) {
                        console.error(err.message);
                    }
                }
            }
            var users = await oracleQuery(`
            SELECT 
                * 
            FROM 
                employees`);
            return res.status(201).json({
                code: 1,
                message: 'OK',
                data: users
            });
        } else {
            return res.status(403).json({
                message: 'FORBIDDEN'
            })
        }
    } catch {
        return res.status(400).send("ข้อมูลไม่ถูกต้อง")
    }
});

app.put('/user/:id', async function (req, res, next) {
    try {
        var headers = req.headers;
        if (headers.authorization == "Bearer 23399cf34d3fd1a3a317c8b86c86cf3b66a4f43f2ddadbe47d2ea880") {
            var id = req.params.id
            var name = req.body.name
            var surname = req.body.surname
            if (typeof name !== "undefined" || typeof surname !== "undefined") {
                return res.status(400).send("ข้อมูลไม่ถูกต้อง")
            }
            var user = await oracleQuery(`
            SELECT 
                * 
            FROM 
                employees
            WHERE
                id = '${id}'`);

            let title = typeof req.body.title === "undefined" || req.body.title === user.rows[0][3] ? ("") : (req.body.title);
            let phone = typeof req.body.phone === "undefined" || req.body.phone === user.rows[0][4] ? ("") : (req.body.phone);
            let email = typeof req.body.email === "undefined" || req.body.email === user.rows[0][5] ? ("") : (req.body.email);

            var emailExist = await exists("email", email)
            var phoneExist = await exists("phone", phone)

            if (!emailExist && !phoneExist) {
                if (title == "") {
                    title = user.rows[0][3]
                }
                if (phone == "") {
                    phone = user.rows[0][4]
                }
                if (email == "") {
                    email = user.rows[0][5]
                }
                try {
                    connection = await oracledb.getConnection(setting);
                    result = await connection.execute("UPDATE employees SET title = (:title), email = (:email), phone = (:phone) WHERE id = (:id)",
                        {
                            title: title,
                            email: email,
                            phone: phone,
                            id: id
                        }, { autoCommit: true });
                    return res.status(200).json({
                        code: 1,
                        message: 'OK',
                        data: users
                    })
                } catch (err) {
                    console.error(err.message);
                } finally {
                    if (connection) {
                        try {
                            await connection.close();
                        } catch (err) {
                            console.error(err.message);
                        }
                    }
                }
                var users = await oracleQuery(`
                SELECT 
                    * 
                FROM 
                    employees`);

                return res.status(200).json({
                    code: 1,
                    message: 'OK',
                    data: users
                })
            } else {
                return res.status(400).send("ข้อมูลไม่ถูกต้อง")
            }
        } else {
            return res.status(403).json({
                message: 'FORBIDDEN'
            })
        }
    } catch {
        return res.status(400).send("ข้อมูลไม่ถูกต้อง")
    }
});

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}`);
    (async function () {
        const url = await ngrok.connect(port);
        console.log(`Ngrok tunnel avalible at ${url}`)
    })();
});
